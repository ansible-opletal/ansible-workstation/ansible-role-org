ansible-role-org
=========

This role installs and configures khal, khard, todoman and vdirsyncer

Requirements
------------

None

Role Variables
--------------

See the variables under vars and defaults, there is not a lot of them

Dependencies
------------

None

Example Playbook
----------------

    - hosts: localhost
      roles:
         - role: ansible-role-org

License
-------

BSD
